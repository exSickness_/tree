#include "bst.h"

bst* bst_create(void)
{
	bst* b = malloc(sizeof(*b));
	if(b) {
		b->root = NULL;
	}
	return b;
}

void bst_destroy(bst* b)
{
	if(!b) {
		return;
	}

	tree_destroy(b->root);
	free(b);
}

bool bst_insert(bst* b, double data)
{

}

bool bst_search(bst* b, double data)
{
	if(!b) {
		return(false);
	}

	return( tree_search(b->root, data) )
}
bool tree_search(struct tree* t, double data)
{
	if(!t) {
		return(false);
	}
	if( fabs(data - t->data) < 1e-3)
	{
		return(true);
	}

	if(data > t->data) {
		return( tree_search(t->right, data) )
	}
	else {
		return( tree_search(t->left, data) )
	}
}

bool bst_remove(bst* b, double data)
{

}
